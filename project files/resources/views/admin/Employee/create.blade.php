@include('admin.header',['mainTitle' => "Creant An Employee"])
	
        <!--Form Wizard-->
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('dashboard/plugins/jquery.steps/css/jquery.steps.css') }}" />

  <!-- Plugins css-->
        <link href="{{ URL::asset('dashboard/plugins/bootstrap-tagsinput/css/bootstrap-tagsinput.css') }}" rel="stylesheet" />
        <link href="{{ URL::asset('dashboard/plugins/bootstrap-select/css/bootstrap-select.min.css') }}" rel="stylesheet" />
        <link href="{{ URL::asset('dashboard/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="{{ URL::asset('dashboard/plugins/switchery/switchery.min.css') }}" />

<style type="text/css">
	#clientbranch{
		display: none;
	}
	.wizard > .content {
		background: white!important;
	}
	.wizard > .steps .current a, .wizard > .steps .current a:hover, .wizard > .steps .current a:active{
		width:100%!important;
		border-radius: 0px;
		background: #02c0ce!important;

	}
	.wizard > .content > .body {
		position: inherit!important;
	}
	.btn-light{
		margin-top: 0px!important;
	}
	#steps-uid-0-t-0,#steps-uid-0-t-1,#steps-uid-0-t-2,#steps-uid-0-t-3{
		width:100%!important;
		border-radius: 0px;

	}
	.wizard > .actions a, .wizard > .actions a:hover, .wizard > .actions a:active{
		    background: #02c0ce!important;
    color: #fff;
    border-radius: 2px!important;
	}
	.buttonsubmit{
			    background: #02c0ce!important;
    color: #fff;
    border-radius: 2px!important;
	}
	.wizard > .actions .disabled a, .wizard > .actions .disabled a:hover, .wizard > .actions .disabled a:active{
		background: #f2f2f2!important;
    color: #313a46!important;

	}
	.wizard > .steps .done a, .wizard > .steps .done a:hover, .wizard > .steps .done a:active{
		    background: #e3eaef!important;
		    color:black;
}
.wizard > .steps {
    height: 58px!important;
}
.steps > ul{
	margin-left: -10px!important;
}
.wizard > .steps .disabled a, .wizard > .steps .disabled a:hover, .wizard > .steps .disabled a:active{
    color: #313a46!important;

}
.Finish{
	display: none!important;
}
</style>
	  <!-- Start Page content -->
                <div class="content">
                    <div class="container-fluid">

                        <!-- Basic Form Wizard -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card-box">
                                    <p class="text-muted m-b-30 font-13">
                                        (*) are Mandatory
                                    </p>

                                    <div class="pull-in">
                                        <form id="basic-form" action="{{route('employees.store')}}" method="post" enctype="multipart/form-data">
                                            <div>
                                            	{{ csrf_field() }}
                                                <h3>Basic Info</h3>
                                                <section>									
                                                	<div class="row">
                                                			<div class="col-md-4">
                                                				<div class="form-group clearfix">
			                                                        <label class="control-label " for="confirm">Full Name *</label>
			                                                        <div class="">
			                                                            <input id="fullname" name="fullname" type="text" class="required form-control">
			                                                        </div>
			                                                    </div>
                                                			</div>
                                                			<div class="col-md-4">
                                                					<div class="form-group clearfix">
				                                                    	 <label class="control-label " for="userName">DOB</label>
				                                                        <div class="">
				                                                            <input type="text" class="form-control" placeholder="yyyy/mm/dd" id="dob" style="width: 80%;float: left;" name="dob">
				                                                                <div class="input-group-append">
				                                                                    <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
				                                                                </div>

				                                                        </div>
				                                                    </div>
                                                			</div>
                                                			<div class="col-md-4">
                                                				<div class="form-group clearfix">
			                                                        <label class="control-label " for="confirm">Gender*</label>
			                                                        <div class="">
			                                                            <input id="gender" name="gender" type="text" class="required form-control">
			                                                        </div>
			                                                    </div>
                                                			</div>
                                                			<div class="col-md-4">
                                                				<div class="form-group clearfix">
			                                                        <label class="control-label " for="confirm">Category</label>
			                                                        <div class="">
			                                                            <input id="Category" name="Category" type="text" class="required form-control">
			                                                        </div>
			                                                    </div>
                                                			</div>
                                                			<div class="col-md-4">
                                                				<div class="form-group clearfix">
			                                                        <label class="control-label " for="confirm">Department*</label>
			                                                       <!--  <div class="">
			                                                            <input id="Department" name="Department" type="text" class="required form-control">
			                                                        </div> -->
			                                                        <div class="departmentss">
			                                                         <select id="Department" class="form-control" name="Department">
			                                                         	<option></option>
			                                                         	@if(Auth::user()->department == "Super Admin")
			                                                         		<option value="Admin">Admin</option>
			                                                         		<option value="Sub Admin">Sub Admin</option>
			                                                         	@elseif(Auth::user()->department == "Admin")
			                                                         		<option value="Sub Admin">Sub Admin</option>
			                                                         	@else
			                                                         		 @if($departments)
							                                                	 @foreach($departments as $department)

																					<option value="{{$department->deptname}}-{{$department->permissions}}">{{$department->deptname}}</option>
							                                            		@endforeach
			                                                         	@endif

			                                                        
				                                            			@endif
				                                            		</select>
				                                            	</div>
			                                                    </div>
                                                			</div>
                                                			<div class="col-md-4">
                                                				<div class="form-group clearfix">
			                                                        <label class="control-label " for="confirm">Designation</label>
			                                                        <div class="">
			                                                            <input id="Designation" name="Designation" type="text" class="required form-control">
			                                                        </div>
			                                                    </div>
                                                			</div>
                                                			<div class="col-md-4">
                                                				<div class="form-group clearfix">
			                                                        <label class="control-label " for="confirm">Email*</label>
			                                                        <div class="">
			                                                            <input id="email" name="email" type="text" class="required form-control">
			                                                        </div>
			                                                    </div>
                                                			</div>
                                                			<div class="col-md-4">
                                                				<div class="form-group clearfix">
			                                                        <label class="control-label " for="confirm">Password*</label>
			                                                        <div class="">
			                                                            <input id="Password" name="Password" type="text" class="required form-control">

			                                                        </div>
			                                                    </div>
                                                			</div>
                                                			<div class="col-md-4">
                                                				<div class="form-group clearfix">
			                                                        <label class="control-label " for="confirm">Branch*</label>
			                                                        
			                                                            <input id="clientbranch" type="text" class="required form-control"
			                                                           
			                                                          
			                                                            >

			                                                        <div class="branches">
			                                                        	@if(Auth::user()->department == "Admin")
				                                                        	@if($branches)
				                                                        		<select id="branchesonload" class="form-control" >
				                                                        			@foreach($branches as $branch)
				                                                        				<?php 
				                                                        					$value = $branch->organisation_id."-".$branch->branchname;

				                                                        				?>
				                                                        				<option value="{{$value}}">
				                                                        					{{$branch->branchname}}
				                                                        				</option>
				                                                        			@endforeach
				                                                        		</select>
				                                                        	@endif
				                                                        @else
				                                                        	<select id="branchesonload" class="form-control" >
				                                                        		<?php
				                                                        			$value = Auth::user()->organisationid."-".Auth::user()->branch;
				                                                        		?>
				                                                        		<option value="{{$value}}">{{Auth::user()->branch}}</option>
				                                                        	</select>
			                                                          	@endif
			                                                        </div>
			                                                    </div>
                                                			</div>
                                                			<div class="col-md-4">
                                                				<div class="form-group clearfix">
			                                                        <label class="control-label " for="confirm">Phone No*</label>
			                                                        <div class="">
			                                                            <input id="phoneno" name="phoneno" type="text" class="required form-control">
			                                                        </div>
			                                                    </div>
                                                			</div>
                                                			<div class="col-md-4">
                                                				<div class="form-group clearfix">
			                                                        <label class="control-label " for="confirm">Alternate No</label>
			                                                        <div class="">
			                                                            <input id="altphoneno" name="altphoneno" type="text" class="required form-control">
			                                                        </div>
			                                                    </div>
                                                			</div>
                                                			<div class="col-md-4">
                                                				<div class="form-group clearfix">
			                                                        <label class="control-label " for="confirm">Employee Type</label>
			                                                        <div class="">
			                                                            <input id="type" name="type" type="text" class="required form-control">
			                                                        </div>
			                                                    </div>
                                                			</div>
                                                			<div class="col-md-4">
                                                				<div class="form-group m-b-0" style="    margin-top: -17px;">
			                                                        <p class="mb-2 mt-4 font-weight-bold">Image</p>
			                                                        <input type="file" class="filestyle" data-placeholder="No file" data-btnClass="btn-light" name="profileimage">
			                                                    </div>
                                                			</div>
                                                			<div class="col-md-4">
                                                				<div class="form-group clearfix">
			                                                        <label class="control-label " for="confirm">Working Hour</label>
			                                                        <div class="">
			                                                            <input id="workinghour" name="workinghour" type="text" class="required form-control">
			                                                        </div>
			                                                    </div>
                                                			</div>
                                                			<div class="col-md-4">
                                                				<div class="form-group clearfix">
			                                                        <label class="control-label " for="confirm">Work Per Week</label>
			                                                        <div class="">
			                                                            <input id="workingperweek" name="workingperweek" type="text" class="required form-control">
			                                                        </div>
			                                                    </div>
                                                			</div>

                                                			<div class="col-md-4">
                                                				<div class="form-group clearfix">
			                                                        <label class="control-label " for="confirm">Supervisor</label>
			                                                        <div class="">
			                                                            <input id="Supervisor" name="Supervisor" type="text" class="required form-control">
			                                                        </div>
			                                                    </div>
                                                			</div>
                                                			<div class="col-md-4">
                                                				<div class="form-group clearfix">
			                                                        <label class="control-label " for="confirm">Organisation</label>
			                                                        <div class="">
			                                                            <input id="Organisation" name="Organisation" type="text" class="required form-control"
			                                                            @if(Auth::user()->department == "Admin")
			                                                            	value="{{Auth::user()->id}}" readonly
			                                                            @elseif(Auth::user()->department == "Super Admin")	
			                                                            @else
			                                                            	value="{{Auth::user()->organisationid}}" readonly
			                                                            @endif
			                                                            >
			                                                        </div>
			                                                    </div>
                                                			</div>
                                                			<div class="col-md-4">
                                                					<div class="form-group clearfix">
				                                                    	 <label class="control-label " for="userName">Joining Date</label>
				                                                        <div class="">
				                                                            <input type="text" class="form-control" placeholder="yyyy/mm/dd" id="datepickerjoiningdate" style="width: 80%;float: left;" name="JoiningDate">
				                                                                <div class="input-group-append">
				                                                                    <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
				                                                                </div>

				                                                        </div>
				                                                    </div>
                                                			</div>
                                                			<div class="col-md-4">
                                                					<div class="checkbox checkbox-primary">
					                                                    <input id="checkbox2" type="checkbox" name="Active">
					                                                    <label for="checkbox2">
					                                                        Active
					                                                    </label>
					                                                </div>
                                                			</div>
                                                	</div>
                                                    
                                                    

                                                    

                                                </section>
                                                <h3>Address and Attachments</h3>
                                                <section>
                                                	<div class="row">
                                                		<div class="col-md-4">
		                                                    <div class="form-group clearfix">
																<label class="control-label" > House No</label>
		                                                        <div class="">
		                                                            <input id="houseno" name="houseno" type="text" class="required form-control" >
		                                                        </div>
		                                                    </div>
		                                                </div>
		                                                <div class="col-md-4">
		                                                    <div class="form-group clearfix">
		                                                        <label class="control-label " >Locality</label>
		                                                        <div class="">
		                                                            <input id="locality" name="locality" type="text" class="required form-control">

		                                                        </div>
		                                                    </div>
		                                                </div>
		                                                <div class="col-md-4">
		                                                    <div class="form-group clearfix">
		                                                        <label class="control-label " >Town/City</label>
		                                                        <div class="">
		                                                            <input id="city" name="city" type="text" class="required email form-control">
		                                                        </div>
		                                                    </div>
		                                                </div>
		                                                <div class="col-md-4">
		                                                    <div class="form-group clearfix">
		                                                        <label class="control-label ">State</label>
		                                                        <div class="">
		                                                            <input id="state" name="state" type="text" class="form-control">
		                                                        </div>
		                                                    </div>
		                                                 </div>
		                                                <div class="col-md-4">
		                                                    <div class="form-group clearfix">
		                                                        <label class="control-label " >Country</label>
		                                                        <div class="">
		                                                            <input id="country" name="country" type="text" class="form-control">
		                                                        </div>
		                                                    </div>
		                                                </div>
		                                                <div class="col-md-4">
		                                                     <div class="form-group clearfix">
		                                                        <label class="control-label " >Pincode</label>
		                                                        <div class="">
		                                                            <input id="pincode" name="pincode" type="text" class="form-control">
		                                                        </div>
		                                                    </div>
		                                                </div>
		                                                 <div class="col-md-4">
		                                                    <div class="form-group clearfix">
		                                                        <label class="control-label " >Qualification</label>
		                                                        <div class="">
		                                                            <input id="Qualification" name="Qualification" type="text" class="form-control">
		                                                        </div>
		                                                    </div>
		                                                 </div>
		                                                <div class="col-md-4">
		                                                    <div class="form-group clearfix">
		                                                        <label class="control-label " >Salary</label>
		                                                        <div class="">
		                                                            <input id="salary" name="salary" type="text" class="form-control">
		                                                        </div>
		                                                    </div>
		                                                </div>
		                                                <div class="col-md-4">
			                                                    <div class="form-group clearfix">
			                                                        <label class="control-label " >Probation Period</label>
			                                                        <div class="">
			                                                            <input id="probationperiod" name="probationperiod" type="text" class="form-control">
			                                                        </div>
			                                                    </div>
			                                            </div>
			                                            <div class="col-md-4">
															<div class="form-group clearfix">
		                                                        <label class="control-label " >Notice Period</label>
		                                                        <div class="">
		                                                            <input id="noticeperiod" name="noticeperiod" type="text" class="form-control">
		                                                        </div>
		                                                    </div>
		                                                </div>
		                                                <div class="col-md-4">
			                                                    <div class="form-group clearfix">
			                                                        <label class="control-label " >NRIC/FIN/Passport No</label>
			                                                        <div class="">
			                                                            <input id="NRIC/FIN/PassportNo" name="NRICFINPassportNo" type="text" class="form-control">
			                                                        </div>
			                                                    </div>
			                                            </div>
			                                            <div class="col-md-4">
		                                                     <div class="form-group clearfix">
		                                                        <label class="control-label " >Employee Tax Ref No</label>
		                                                        <div class="">
		                                                            <input id="EmployeeTaxRefNo" name="EmployeeTaxRefNo" type="text" class="form-control">
		                                                        </div>
		                                                    </div>
		                                                </div>
		                                                <div class="col-md-4">
		                                                     <div class="form-group clearfix">
		                                                        <label class="control-label " >Employee PF No</label>
		                                                        <div class="">
		                                                            <input id="EmployeeCPFAccountNo" name="EmployeeCPFAccountNo" type="text" class="form-control">
		                                                        </div>
		                                                    </div>
		                                                </div>
		                                                <div class="col-md-4">
		                                                    <div class="form-group clearfix">
		                                                       
		                                                				<div class="form-group m-b-0" style="    margin-top: -17px;">
					                                                        <p class="mb-2 mt-4 font-weight-bold">Identity Proof</p>
					                                                        <input type="file" class="filestyle" data-placeholder="No file" data-btnClass="btn-light" name="identityproof">
					                                                    </div>
		                                                		
		                                                    </div>
		                                                </div>
		                                                <div class="col-md-4">
		                                                    <div class="form-group clearfix">
		                                                				<div class="form-group m-b-0" style="    margin-top: -17px;">
					                                                        <p class="mb-2 mt-4 font-weight-bold">Address Proof</p>
					                                                        <input type="file" class="filestyle" data-placeholder="No file" data-btnClass="btn-light" name="addressproof">
					                                                    </div>
		                                                			</div>
		                                                    
		                                                </div>

                                                  </div>

                                                </section>
                                                <h3>Bank Details</h3>
                                                <section>
                                                    <div class="row">
                                                    	<div class="col-md-4">
		                                                     <div class="form-group clearfix">
		                                                        <label class="control-label " >Name</label>
		                                                        <div class="">
		                                                            <input id="bankname" name="bankname" type="text" class="form-control">
		                                                        </div>
		                                                    </div>
		                                                </div>
		                                                <div class="col-md-4">
		                                                     <div class="form-group clearfix">
		                                                        <label class="control-label " >Account No</label>
		                                                        <div class="">
		                                                            <input id="accountno" name="accountno" type="text" class="form-control">
		                                                        </div>
		                                                    </div>
		                                                </div>
		                                                <div class="col-md-4">
		                                                     <div class="form-group clearfix">
		                                                        <label class="control-label " >Branch</label>
		                                                        <div class="">
		                                                            <input id="bankbranch" name="bankbranch" type="text" class="form-control">
		                                                        </div>
		                                                    </div>
		                                                </div>
		                                                <div class="col-md-4">
		                                                     <div class="form-group clearfix">
		                                                        <label class="control-label " >Swift Code</label>
		                                                        <div class="">
		                                                            <input id="swiftcode" name="swiftcode" type="text" class="form-control">
		                                                        </div>
		                                                    </div>
		                                                </div>
		                                                 <div class="col-md-4">
		                                                     <div class="form-group clearfix">
		                                                        <label class="control-label " >City</label>
		                                                        <div class="">
		                                                            <input id="bcity" name="bcity" type="text" class="form-control">
		                                                        </div>
		                                                    </div>
		                                                </div>
                                                    </div>
                                                </section>
                                                <h3>Permissions</h3>
                                                <section>
                                                	<div class="row PermissnsSet">
                                                     <div class="col-md-4">
                                                     	<label> Employees</label>

		                                                    <div class="checkbox checkbox-success checkbox-circle">
			                                                    <input id="checkbox-10" class="checkboxss" type="checkbox" value="1" >
			                                                    <label for="checkbox-10">
			                                                        Create
			                                                    </label>
			                                                </div>
		                                                    <div class="checkbox checkbox-success checkbox-circle">
			                                                    <input id="checkbox-11" class="checkboxss"  type="checkbox" value="3">
			                                                    <label for="checkbox-11" >
			                                                        Edit
			                                                    </label>
			                                                </div>
		                                                    <div class="checkbox checkbox-success checkbox-circle">
			                                                    <input id="checkbox-12" class="checkboxss" type="checkbox" value="4" >
			                                                    <label for="checkbox-12">
			                                                        Delete
			                                                    </label>
			                                                </div>
		                                                    <div class="checkbox checkbox-success checkbox-circle">
			                                                    <input id="checkbox-13" class="checkboxss" type="checkbox" value="2" >
			                                                    <label for="checkbox-13">
			                                                        View
			                                                    </label>
			                                                </div>
		                                                </div>
		                                               <div class="col-md-4">
                                                     	<label> Departments</label>

		                                                    <div class="checkbox checkbox-success checkbox-circle">
			                                                    <input id="checkbox-6" class="checkboxss" type="checkbox" value="6" >
			                                                    <label for="checkbox-6">
			                                                        Create
			                                                    </label>
			                                                </div>
		                                                    <div class="checkbox checkbox-success checkbox-circle">
			                                                    <input id="checkbox-7" class="checkboxss" type="checkbox" value="7" >
			                                                    <label for="checkbox-7">
			                                                        Edit
			                                                    </label>
			                                                </div>
		                                                    <div class="checkbox checkbox-success checkbox-circle">
			                                                    <input id="checkbox-8" class="checkboxss" type="checkbox" value="8" >
			                                                    <label for="checkbox-8">
			                                                        Delete
			                                                    </label>
			                                                </div>
		                                                    <div class="checkbox checkbox-success checkbox-circle">
			                                                    <input id="checkbox-5" class="checkboxss" type="checkbox" value="5" >
			                                                    <label for="checkbox-5">
			                                                        View
			                                                    </label>
			                                                </div>
		                                                </div>
		                                                <div class="col-md-4">
                                                     	<label> Branches</label>

		                                                    <div class="checkbox checkbox-success checkbox-circle">
			                                                    <input id="checkbox-101" class="checkboxss" type="checkbox" value="10" >
			                                                    <label for="checkbox-101">
			                                                        Create
			                                                    </label>
			                                                </div>
		                                                    <div class="checkbox checkbox-success checkbox-circle">
			                                                    <input id="checkbox-111" class="checkboxss" type="checkbox" value="11" >
			                                                    <label for="checkbox-111">
			                                                        Edit
			                                                    </label>
			                                                </div>
		                                                    <div class="checkbox checkbox-success checkbox-circle">
			                                                    <input id="checkbox-121" class="checkboxss" type="checkbox" value="12" >
			                                                    <label for="checkbox-121">
			                                                        Delete
			                                                    </label>
			                                                </div>
		                                                    <div class="checkbox checkbox-success checkbox-circle">
			                                                    <input id="checkbox-99" class="checkboxss" type="checkbox" value="9" >
			                                                    <label for="checkbox-99">
			                                                        View
			                                                    </label>
			                                                </div>
		                                                </div>
		                                                <div class="col-md-4">
                                                     	<label> Employee Type</label>

		                                                    <div class="checkbox checkbox-success checkbox-circle">
			                                                    <input id="checkbox-1234" class="checkboxss" type="checkbox" value="15" >
			                                                    <label for="checkbox-1234">
			                                                        Create
			                                                    </label>
			                                                </div>
		                                                    <div class="checkbox checkbox-success checkbox-circle">
			                                                    <input id="checkbox-1435" class="checkboxss" type="checkbox" value="14" >
			                                                    <label for="checkbox-1435">
			                                                        Edit
			                                                    </label>
			                                                </div>
		                                                    <div class="checkbox checkbox-success checkbox-circle">
			                                                    <input id="checkbox-1578" class="checkboxss" type="checkbox" value="16" >
			                                                    <label for="checkbox-1578">
			                                                        Delete
			                                                    </label>
			                                                </div>
		                                                    <div class="checkbox checkbox-success checkbox-circle">
			                                                    <input id="checkbox-1289" class="checkboxss" type="checkbox" value="13" >
			                                                    <label for="checkbox-1289">
			                                                        View
			                                                    </label>
			                                                </div>
		                                                </div>

		                                               </div>
		                                              <div class="row Packages">
		                                              	<div class="col-md-4">
		                                              			 <div class="checkbox checkbox-success checkbox-circle">
				                                                    <input id="checkbox-123890" class="Freetrial" type="checkbox" value="1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16" >
				                                                    <label for="checkbox-123890" >
				                                                        Free Trail
				                                                    </label>
			                                              		</div>
		                                              	</div>
		                                              	<div class="col-md-4">
		                                              			 <div class="form-group">
			                                                        <label>Starting from</label>
			                                                        <div>
			                                                            <div class="input-group">
			                                                                <input type="text" class="form-control" placeholder="mm/dd/yyyy" id="packagesatrtdate" name="packagesatrtdate">
			                                                                <div class="input-group-append">
			                                                                    <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
			                                                                </div>
			                                                            </div>
			                                                        </div>
			                                                    </div>
		                                              	</div>
		                                              	<div class="col-md-4">
		                                              			 <div class="form-group">
			                                                        <label>Till</label>
			                                                        <div>
			                                                            <div class="input-group">
			                                                                <input type="text" class="form-control" placeholder="mm/dd/yyyy" id="packageenddate" name="packageenddate">
			                                                                <div class="input-group-append">
			                                                                    <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
			                                                                </div>
			                                                            </div>
			                                                        </div>
			                                                    </div>
		                                              	</div>
		                                              </div>
                                                </section>
                                                
                                            </div>
                                        <input type="hidden" name="Permissions" id="Permissions">
                                        <input type="hidden" name="clientbranch" id="clientbranchfinal">
                                        </form>
                                    </div> 

                                </div>
                            </div>
                        </div>

                        <!-- End row -->

                  </div>

               </div>

@include('admin.footer') 


        <!--Form Wizard-->
        <script src="{{ URL::asset('dashboard/plugins/jquery.steps/js/jquery.steps.min.js') }}" type="text/javascript"></script>

        <!--wizard initialization-->
        <script src="{{ URL::asset('dashboard/assets/pages/jquery.wizard-init.js') }}" type="text/javascript"></script>


             <!-- plugin js -->
        <script src="{{ URL::asset('dashboard/plugins/moment/moment.js') }}"></script>
        <script src="{{ URL::asset('dashboard/plugins/bootstrap-timepicker/bootstrap-timepicker.js') }}"></script>
        <script src="{{ URL::asset('dashboard/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js') }}"></script>
        <script src="{{ URL::asset('dashboard/plugins/clockpicker/js/bootstrap-clockpicker.min.js') }}"></script>
        <script src="{{ URL::asset('dashboard/assets/js/bootstrap.bundle.min.js') }}"></script>
        <script src="{{ URL::asset('dashboard/plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
        <script src="{{ URL::asset('dashboard/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>


        <script src="{{ URL::asset('dashboard/plugins/switchery/switchery.min.js') }}"></script>
        <script src="{{ URL::asset('dashboard/plugins/bootstrap-tagsinput/js/bootstrap-tagsinput.min.js') }}"></script>
        <script src="{{ URL::asset('dashboard/plugins/select2/js/select2.min.js') }}" type="text/javascript"></script>
        <script src="{{ URL::asset('dashboard/plugins/bootstrap-select/js/bootstrap-select.js') }}" type="text/javascript"></script>
        <script src="{{ URL::asset('dashboard/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js') }}" type="text/javascript"></script>
        <script src="{{ URL::asset('dashboard/plugins/bootstrap-maxlength/bootstrap-maxlength.js') }}" type="text/javascript"></script>

        <script type="text/javascript" src="{{ URL::asset('dashboard/plugins/autocomplete/jquery.mockjax.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('dashboard/plugins/autocomplete/jquery.autocomplete.min.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('dashboard/plugins/autocomplete/countries.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('dashboard/assets/pages/jquery.autocomplete.init.js') }}"></script>

        <!-- Init js -->
        <script src="{{ URL::asset('dashboard/assets/pages/jquery.form-pickers.init.js') }}"></script>
        <script type="text/javascript">
            $(document).ready(function() {
            		
              	
            		$(".actions ul").append('<li><input type="submit" value="Submit" class="submit btn btn-custom waves-light waves-effect"></li>');
              		$(".submit").click(function(event){
  							// event.preventDefault();
  							let department = $("#Department").val();
  							if(department == "Admin"){
  								let value = $("#clientbranch").val();
  								$("#clientbranchfinal").val(value);

  							}else{
  								let value = $("#branchesonload").val();
  								value = value.split("-")[1];
  								$("#clientbranchfinal").val(value);
  							}

					});

              $('#packagesatrtdate').datepicker({
    format: 'yyyy/mm/dd',
    todayHighlight:'TRUE',
    autoclose: true,
})           	
              $('#packageenddate').datepicker({
    format: 'yyyy/mm/dd',
    todayHighlight:'TRUE',
    autoclose: true,
})
              $('#datepickerjoiningdate').datepicker({
    format: 'yyyy/mm/dd',
    todayHighlight:'TRUE',
    autoclose: true,
})
              $('#dob').datepicker({
    format: 'yyyy/mm/dd',
    todayHighlight:'TRUE',
    autoclose: true,
})

               let values = [];
              $("#Department").change(function(event){
              		let value = $(this).val();
              		if(value == "Admin" || value == "admin"){
              			$("#clientbranch").show();
              			$(".Packages").show();
              			$(".PermissnsSet").hide();
              			$(".branches").empty();
              			$("#Organisation").val("");
              		}else{
              			$(".PermissnsSet").show();
              			$(".Packages").hide();
              			$("#clientbranch").hide();
              		}
              		if(value == "Admin" || value == "admin" || value == "Sub Admin"){
              		}else{
              			if(value){
              		
              				let permissionsset = value.split("-");
              					$("#Permissions").val(permissionsset[1]);

              				 permissionsset = permissionsset[1].split(",");
              				 values =permissionsset;
		              		for(let k = 0 ; k < permissionsset.length ; k++){
		              			 $("input[value='" + permissionsset[k] + "']").prop('checked', true);
		              		}
              		}else{
              			$('.checkboxss').prop('checked', false);
              			$("#Permissions").val("");
              			values = []
              		}
              		}
              		
              	
					  
               
               });
             

              $('#Organisation').change(function() {
              		let id = $(this).val();
              		$("#clientbranch").hide();
     				if(id){
						    var xhttp = new XMLHttpRequest();
						  xhttp.onreadystatechange = function() {
						    if (this.readyState == 4 && this.status == 200) {
						   
						     $(".branches").html(this.responseText)
						    }
						  };
						  xhttp.open("GET", "http://127.0.0.1:8000/branch.php?str="+id, true);
						  xhttp.send();
     				}else{
     					$(".branches").empty();
     				}



              	 });
                $('.checkboxss').change(function() {

       				$(this).each(function(){
  						if($(this).is(':checked')){
  							let n = values.includes($(this).attr('value'));
  							if(!n){
  									values.push($(this).attr('value'));
  							}
  						
    					}else{
    						let remvoevalue = $(this).attr('value');
    						values = $.grep(values, function(value) {
							  return value != remvoevalue;
							});
    					}

    					let finalvalues = values.toString();
    					$("#Permissions").val(finalvalues);
 				});
               
    		});

			 $('.Freetrial').change(function() {
			 		
			 		if($(this).is(':checked')){
			 				let remvoevalue = $(this).attr('value');
			 				let finalvalues = remvoevalue.toString();
    						$("#Permissions").val(finalvalues);
			 		}
			 });


			 //Admin Login functions
			 	$('#branchesonload').change(function() {
             	let value = $(this).val();
             	let valuesplit = value.split("-");
             	let organisation_id = valuesplit[0];
             	let branchselected = valuesplit[1];

             	 var xhttp = new XMLHttpRequest();
						  xhttp.onreadystatechange = function() {
						    if (this.readyState == 4 && this.status == 200) {
						   
						     
						     $(".departmentss").html(this.responseText);
						     let department = "<?php echo Auth::user()->department;?>";
						     if(department == "Admin"){
						     	$("#Department option[value='Admin']").remove();
						     }
						    
						    }
						  };
						  xhttp.open("GET", "http://127.0.0.1:8000/departments.php?organisation_id="+organisation_id+"&brnach="+branchselected, true);
						  xhttp.send();
             });	
            } );

        </script>

@include('admin.header',['mainTitle' => "Departments"])
<!-- DataTables -->
        <link href="{{ URL::asset('dashboard/plugins/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('dashboard/plugins/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- Responsive datatable examples -->
        <link href="{{ URL::asset('dashboard/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />

        <!-- Multi Item Selection examples -->
        <link href="{{ URL::asset('dashboard/plugins/datatables/select.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<style type="text/css">
	.addnew{
		text-align: right;
		margin-bottom: 20px;
	}
</style>

        		  <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-12">

                                <div class="card-box table-responsive">
                                 @if(in_array("5", $permissionset))
                                	<div class="addnew">
                                		 <div class="button-list">
                                		 	
                                		 		<a href="/departments/create"  class="btn btn-primary waves-light waves-effect">Add </a>
                                		 </div>
                                	</div>
                                @endif
                                    <table id="responsive-datatable" class="table table-bordered table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                        <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Permissions</th>
                                            <th>Branch</th>
                                            <th>Organisation</th>
                                              <th>Actions</th>
                                        </tr>
                                        </thead>


                                        <tbody>
                                        	@foreach($departments as $department)
                                        		 	<tr>
			                                            <td>{{$department->deptname}}</td>
			                                            <td>{{$department->permissions}}</td>
			                                            <td>{{$department->branch}}</td>
			                                            <td>{{$department->name}}</td>
                                                        <td><a href="{{route('departments.edit',$department->id)}}"><i class="fa fa-edit"></i> </a> <i class="fa fa-trash-o"></i></td>
			                                        </tr>
                                        	@endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div> <!-- end row -->

                 </div>
              </div>

@include('admin.footer') 

        <!-- Required datatable js -->
        <script src="{{ URL::asset('dashboard/plugins/datatables/jquery.dataTables.min.js') }}"></script>
        <script src="{{ URL::asset('dashboard/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
        <!-- Buttons examples -->
        <script src="{{ URL::asset('dashboard/plugins/datatables/dataTables.buttons.min.js') }}"></script>
        <script src="{{ URL::asset('dashboard/plugins/datatables/buttons.bootstrap4.min.js') }}"></script>
        <script src="{{ URL::asset('dashboard/plugins/datatables/jszip.min.js') }}"></script>
        <script src="{{ URL::asset('dashboard/plugins/datatables/pdfmake.min.js') }}"></script>
        <script src="{{ URL::asset('dashboard/plugins/datatables/vfs_fonts.js') }}"></script>
        <script src="{{ URL::asset('dashboard/plugins/datatables/buttons.html5.min.js') }}"></script>
        <script src="{{ URL::asset('dashboard/plugins/datatables/buttons.print.min.js') }}"></script>

        <!-- Key Tables -->
        <script src="{{ URL::asset('dashboard/plugins/datatables/dataTables.keyTable.min.js') }}"></script>

        <!-- Responsive examples -->
        <script src="{{ URL::asset('dashboard/plugins/datatables/dataTables.responsive.min.js') }}"></script>
        <script src="{{ URL::asset('dashboard/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>

        <!-- Selection table -->
        <script src="{{ URL::asset('dashboard/plugins/datatables/dataTables.select.min.js') }}"></script>

        <!-- App js -->
        <script src="{{ URL::asset('dashboard/assets/js/jquery.core.js') }}"></script>
        <script src="{{ URL::asset('dashboard/assets/js/jquery.app.js') }}"></script>

        <script type="text/javascript">
            $(document).ready(function() {

              

                // Responsive Datatable
                $('#responsive-datatable').DataTable();

              
            } );

        </script>




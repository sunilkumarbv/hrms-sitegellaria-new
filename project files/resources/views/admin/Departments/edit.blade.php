@include('admin.header',['mainTitle' => "Edit An Department"])

        <!--Form Wizard-->
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('dashboard/plugins/jquery.steps/css/jquery.steps.css') }}" />

  <!-- Plugins css-->
        <link href="{{ URL::asset('dashboard/plugins/bootstrap-tagsinput/css/bootstrap-tagsinput.css') }}" rel="stylesheet" />
        <link href="{{ URL::asset('dashboard/plugins/bootstrap-select/css/bootstrap-select.min.css') }}" rel="stylesheet" />
        <link href="{{ URL::asset('dashboard/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="{{ URL::asset('dashboard/plugins/switchery/switchery.min.css') }}" />
<style type="text/css">
	.wizard > .content {
		background: white!important;
	}
	.wizard > .steps .current a, .wizard > .steps .current a:hover, .wizard > .steps .current a:active{
		width:100%!important;
		border-radius: 0px;
		background: #02c0ce!important;

	}
	.wizard > .content > .body {
		position: inherit!important;
	}
	.btn-light{
		margin-top: 0px!important;
	}
	#steps-uid-0-t-0,#steps-uid-0-t-1,#steps-uid-0-t-2,#steps-uid-0-t-3{
		width:100%!important;
		border-radius: 0px;

	}
	.wizard > .actions a, .wizard > .actions a:hover, .wizard > .actions a:active{
		    background: #02c0ce!important;
    color: #fff;
    border-radius: 2px!important;
	}
	.buttonsubmit{
			    background: #02c0ce!important;
    color: #fff;
    border-radius: 2px!important;
	}
	.wizard > .actions .disabled a, .wizard > .actions .disabled a:hover, .wizard > .actions .disabled a:active{
		background: #f2f2f2!important;
    color: #313a46!important;

	}
	.wizard > .steps .done a, .wizard > .steps .done a:hover, .wizard > .steps .done a:active{
		    background: #e3eaef!important;
		    color:black;
}
.wizard > .steps {
    height: 58px!important;
}
.steps > ul{
	margin-left: -10px!important;
}
.wizard > .steps .disabled a, .wizard > .steps .disabled a:hover, .wizard > .steps .disabled a:active{
    color: #313a46!important;

}
.Finish{
	display: none!important;
}
</style>
	  <!-- Start Page content -->
                <div class="content">
                    <div class="container-fluid">

                        <!-- Basic Form Wizard -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card-box">
                                    <p class="text-muted m-b-30 font-13">
                                        (*) are Mandatory
                                    </p>

                                    <div class="pull-in">
                                        <form id="basic-form" action="{{route('departments.update',$department->id)}}" method="post" enctype="multipart/form-data">
                                            <div>
                                            	{{ csrf_field() }}
                                                {{ method_field('PATCH') }}
                                                <h3>Department Info</h3>
                                                <section>									
                                                	<div class="row">
                                                		<div class="col-md-4">
                                                				<div class="form-group clearfix">
			                                                        <label class="control-label " for="confirm">Name *</label>
			                                                        <div class="">
			                                                            <input id="deptname" name="deptname" type="text" class="required form-control" value="{{$department->deptname}}">
			                                                        </div>
			                                                    </div>
                                                		</div>
                                                		<div class="col-md-4">
                                                				<div class="form-group clearfix">
			                                                        <label class="control-label " for="confirm">Code *</label>
			                                                        <div class="">
			                                                            <input id="deptcode" name="deptcode" type="text" class="required form-control" value="{{$department->code}}">
			                                                        </div>
			                                                    </div>
                                                		</div>
                                                		
                                                		
                                                		
                                                		<div class="col-md-4">
                                                				<div class="form-group clearfix">
			                                                        <label class="control-label " for="confirm"> Organisation </label>
			                                                        <div class="">

			                                                        @if(Auth::user()->department == "Super Admin")
                                                                            <input id="Organisationid" name="Organisationid" type="text" class="required form-control" value="">
                                                                        @elseif(Auth::user()->department == "Admin")
                                                                            <input id="Organisationid" name="Organisationid" type="text" class="required form-control" value="{{Auth::user()->id}}" readonly>
                                                                        @elseif(Auth::user()->department == "Sub Admin")
                                                                            <input id="Organisationid" name="Organisationid" type="text" class="required form-control" value="{{Auth::user()->organisationid}}" readonly>
                                                                            @else
                                                                            <input id="Organisationid" name="Organisationid" type="text" class="required form-control" value="{{Auth::user()->organisationid}}" readonly>
                                                                        @endif
			                                                            
			                                                        </div>
			                                                    </div>
                                                		</div>
                                                		<div class="col-md-4 branchs">
                                                                <div class="form-group clearfix">
                                                                    <div class="form-group">
                                                                      <label for="sel1">Branch *</label>
                                                                      <select class="form-control"  name="branch">
                                                                        @foreach($branches as $branch)
                                                                        <option value="{{$branch->branchname}}" @if($department->branch == $branch->branchname)

                                                                            selected 
                                                                        @endif>{{$branch->branchname}}</option>
                                                                        @endforeach
                                                                      </select>
                                                                    </div>
                                                                </div>
                                                        </div>
                                                			
                                                    <div class="col-md-12"></div><br>
                                                	<div class="col-md-12">
                                                         <div class="form-group clearfix">
                                                            <label class="control-label " for="confirm">Permissions</label>
                                                         </div>   
                                                    </div>
                                                     <div class="col-md-4">
                                                        <label> Employees</label>
                                                        <?php 

                                                        $permissionsetexploded = explode(",", $department->permissions);
                                                        ?>
                                                            <div class="checkbox checkbox-success checkbox-circle">
                                                                <input id="checkbox-10" class="checkboxss" type="checkbox" value="1" 
                                                                @if(in_array("1", $permissionsetexploded))
                                                                    checked 
                                                                @endif
                                                                >
                                                                <label for="checkbox-10">
                                                                    Create
                                                                </label>
                                                            </div>
                                                            <div class="checkbox checkbox-success checkbox-circle">
                                                                <input id="checkbox-11" class="checkboxss"  type="checkbox" value="3" 
                                                                @if(in_array("3", $permissionsetexploded))
                                                                    checked 
                                                                @endif>
                                                                <label for="checkbox-11"  
                                                                >
                                                                    Edit
                                                                </label>
                                                            </div>
                                                            <div class="checkbox checkbox-success checkbox-circle">
                                                                <input id="checkbox-12" class="checkboxss" type="checkbox" value="4" 
                                                                 @if(in_array("4", $permissionsetexploded))
                                                                    checked 
                                                                @endif>
                                                                <label for="checkbox-12">
                                                                    Delete
                                                                </label>
                                                            </div>
                                                            <div class="checkbox checkbox-success checkbox-circle">
                                                                <input id="checkbox-13" class="checkboxss" type="checkbox" value="2" 
                                                                 @if(in_array("2", $permissionsetexploded))
                                                                    checked 
                                                                @endif>
                                                                <label for="checkbox-13">
                                                                    View
                                                                </label>
                                                            </div>
                                                        </div>
                                                       <div class="col-md-4">
                                                        <label> Departments</label>

                                                            <div class="checkbox checkbox-success checkbox-circle">
                                                                <input id="checkbox-6" class="checkboxss" type="checkbox" value="6"  
                                                                @if(in_array("6", $permissionsetexploded))
                                                                    checked 
                                                                @endif>
                                                                <label for="checkbox-6">
                                                                    Create
                                                                </label>
                                                            </div>
                                                            <div class="checkbox checkbox-success checkbox-circle">
                                                                <input id="checkbox-7" class="checkboxss" type="checkbox" value="7" 
                                                                 @if(in_array("7", $permissionsetexploded))
                                                                    checked 
                                                                @endif>
                                                                <label for="checkbox-7">
                                                                    Edit
                                                                </label>
                                                            </div>
                                                            <div class="checkbox checkbox-success checkbox-circle">
                                                                <input id="checkbox-8" class="checkboxss" type="checkbox" value="8" 
                                                                 @if(in_array("8", $permissionsetexploded))
                                                                    checked 
                                                                @endif>
                                                                <label for="checkbox-8">
                                                                    Delete
                                                                </label>
                                                            </div>
                                                            <div class="checkbox checkbox-success checkbox-circle">
                                                                <input id="checkbox-5" class="checkboxss" type="checkbox" value="5" 
                                                                 @if(in_array("5", $permissionsetexploded))
                                                                    checked 
                                                                @endif>
                                                                <label for="checkbox-5">
                                                                    View
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                        <label> Branches</label>

                                                            <div class="checkbox checkbox-success checkbox-circle">
                                                                <input id="checkbox-101" class="checkboxss" type="checkbox" value="10" 
                                                                 @if(in_array("10", $permissionsetexploded))
                                                                    checked 
                                                                @endif>
                                                                <label for="checkbox-101">
                                                                    Create
                                                                </label>
                                                            </div>
                                                            <div class="checkbox checkbox-success checkbox-circle">
                                                                <input id="checkbox-111" class="checkboxss" type="checkbox" value="11" 
                                                                 @if(in_array("11", $permissionsetexploded))
                                                                    checked 
                                                                @endif>
                                                                <label for="checkbox-111">
                                                                    Edit
                                                                </label>
                                                            </div>
                                                            <div class="checkbox checkbox-success checkbox-circle">
                                                                <input id="checkbox-121" class="checkboxss" type="checkbox" value="12" 
                                                                 @if(in_array("12", $permissionsetexploded))
                                                                    checked 
                                                                @endif>
                                                                <label for="checkbox-121">
                                                                    Delete
                                                                </label>
                                                            </div>
                                                            <div class="checkbox checkbox-success checkbox-circle">
                                                                <input id="checkbox-99" class="checkboxss" type="checkbox" value="9" 
                                                                 @if(in_array("9", $permissionsetexploded))
                                                                    checked 
                                                                @endif>
                                                                <label for="checkbox-99">
                                                                    View
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                        <label> Employee Type</label>

                                                            <div class="checkbox checkbox-success checkbox-circle">
                                                                <input id="checkbox-1234" class="checkboxss" type="checkbox" value="15" 
                                                                 @if(in_array("15", $permissionsetexploded))
                                                                    checked 
                                                                @endif>
                                                                <label for="checkbox-1234">
                                                                    Create
                                                                </label>
                                                            </div>
                                                            <div class="checkbox checkbox-success checkbox-circle">
                                                                <input id="checkbox-1435" class="checkboxss" type="checkbox" value="14" 
                                                                 @if(in_array("14", $permissionsetexploded))
                                                                    checked 
                                                                @endif>
                                                                <label for="checkbox-1435">
                                                                    Edit
                                                                </label>
                                                            </div>
                                                            <div class="checkbox checkbox-success checkbox-circle">
                                                                <input id="checkbox-1578" class="checkboxss" type="checkbox" value="16" 
                                                                 @if(in_array("16", $permissionsetexploded))
                                                                    checked 
                                                                @endif>
                                                                <label for="checkbox-1578">
                                                                    Delete
                                                                </label>
                                                            </div>
                                                            <div class="checkbox checkbox-success checkbox-circle">
                                                                <input id="checkbox-1289" class="checkboxss" type="checkbox" value="13" 
                                                                 @if(in_array("13", $permissionsetexploded))
                                                                    checked 
                                                                @endif>
                                                                <label for="checkbox-1289">
                                                                    View
                                                                </label>
                                                            </div>
                                                        </div>
                                                </div>
                                            </section>
                                              
                                                    <input type="hidden" name="Permissions" id="Permissions" value="{{$department->permissions}}">
                                                   
                                            </div>
                                        </form>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <!-- End row -->

                  </div>

               </div>

@include('admin.footer') 


        <!--Form Wizard-->
        <script src="{{ URL::asset('dashboard/plugins/jquery.steps/js/jquery.steps.min.js') }}" type="text/javascript"></script>

        <!--wizard initialization-->
        <script src="{{ URL::asset('dashboard/assets/pages/jquery.wizard-init.js') }}" type="text/javascript"></script>



        <script src="{{ URL::asset('dashboard/plugins/switchery/switchery.min.js') }}"></script>
        <script src="{{ URL::asset('dashboard/plugins/bootstrap-tagsinput/js/bootstrap-tagsinput.min.js') }}"></script>
        <script src="{{ URL::asset('dashboard/plugins/select2/js/select2.min.js') }}" type="text/javascript"></script>
        <script src="{{ URL::asset('dashboard/plugins/bootstrap-select/js/bootstrap-select.js') }}" type="text/javascript"></script>
        <script src="{{ URL::asset('dashboard/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js') }}" type="text/javascript"></script>
        <script src="{{ URL::asset('dashboard/plugins/bootstrap-maxlength/bootstrap-maxlength.js') }}" type="text/javascript"></script>

        <script type="text/javascript" src="{{ URL::asset('dashboard/plugins/autocomplete/jquery.mockjax.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('dashboard/plugins/autocomplete/jquery.autocomplete.min.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('dashboard/plugins/autocomplete/countries.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('dashboard/assets/pages/jquery.autocomplete.init.js') }}"></script>

        <!-- Init js -->
        <script src="{{ URL::asset('dashboard/assets/pages/jquery.form-pickers.init.js') }}""></script>
         <script src="{{ URL::asset('js/ajaxcall.js') }}" type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function() {
            		$(".submit").click(function(event){
  							event.preventDefault();

					});
              	
            		$(".actions ul").append('<li><input type="submit" value="Submit" class="submit btn btn-custom waves-light waves-effect"></li>');
              	
                    let values = [];    
                    let valuesofselectedpermissions = "<?php echo $department->permissions;?>";
                    values = valuesofselectedpermissions.split(",");
                    $('.checkboxss').change(function() {
                        $(this).each(function(){
                            if($(this).is(':checked')){
                                values.push($(this).attr('value'))
                            }else{
                                let remvoevalue = $(this).attr('value');
                                values = $.grep(values, function(value) {
                                  return value != remvoevalue;
                                });
                            }
                            let finalvalues = values.toString();
                            $("#Permissions").val(finalvalues);
                    });
                   
                });
             
                  $('#Organisationid').change( function() {
                    let value = $(this).val();
                    $.get("{{ URL::to('getorganiationbranches') }}", { id : value},function(data){
                       $(".branchs").html(data);
                      });
   
                    console.log("done");

              
             
            } );
                  
            } );

        </script>

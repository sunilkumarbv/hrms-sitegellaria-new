
@include('admin.header',['mainTitle' => "Dashboard"])
             <div class="content">
                    <div class="container-fluid">

                        <div class="row" style="">
                            <div class="col-12">
                                <div class="card-box">
                                    <h4 class="header-title mb-4">Overview</h4>

                                    <div class="row" style="    margin-top: -29px;margin-bottom: -20px;">
                                        @if(Auth::user()->department == "Super Admin" || Auth::user()->department == "Admin")
                                        <div class="col-sm-6 col-lg-6 col-xl-3">
                                            <div class="card-box mb-0 widget-chart-two">
                                                <div class="float-right" style="margin-top: 18px;margin-right: 38px;">
                                                 <i class="icon-user" style=" color: rgb(10, 207, 151);   font-size: 40px;"></i>
                                                </div>
                                                <div class="widget-chart-two-content">
                                                    <p class="text-muted mb-0 mt-2">Total Admins</p>
                                                    <h3 class="">12</h3>
                                                </div>

                                            </div>
                                        </div>
                                        @endif
                                        @if(Auth::user()->department == "Super Admin" || Auth::user()->department == "Admin" || Auth::user()->department == "Sub Admin")
                                        <div class="col-sm-6 col-lg-6 col-xl-3">
                                            <div class="card-box mb-0 widget-chart-two">
                                                <div class="float-right" style="margin-top: 18px;margin-right: 38px;">
                                                 <i class="icon-people" style="     color: rgb(241, 85, 108);   font-size: 40px;"></i>
                                                </div>
                                                <div class="widget-chart-two-content">
                                                    <p class="text-muted mb-0 mt-2">Total Employees</p>
                                                    <h3 class="">12</h3>
                                                </div>

                                            </div>
                                        </div>
                                    @endif
                                    </div>
                                    <!-- end row -->
                                </div>
                            </div>
                        </div>
                        <!-- end row -->
                    </div>
                </div>

@include('admin.footer')       
@include('admin.header',['mainTitle' => "Creant An Employee"])

        <!--Form Wizard-->
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('dashboard/plugins/jquery.steps/css/jquery.steps.css') }}" />

  <!-- Plugins css-->
        <link href="{{ URL::asset('dashboard/plugins/bootstrap-tagsinput/css/bootstrap-tagsinput.css') }}" rel="stylesheet" />
        <link href="{{ URL::asset('dashboard/plugins/bootstrap-select/css/bootstrap-select.min.css') }}" rel="stylesheet" />
        <link href="{{ URL::asset('dashboard/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="{{ URL::asset('dashboard/plugins/switchery/switchery.min.css') }}" />
<style type="text/css">
	.wizard > .content {
		background: white!important;
	}
	.wizard > .steps .current a, .wizard > .steps .current a:hover, .wizard > .steps .current a:active{
		width:100%!important;
		border-radius: 0px;
		background: #02c0ce!important;

	}
	.wizard > .content > .body {
		position: inherit!important;
	}
	.btn-light{
		margin-top: 0px!important;
	}
	#steps-uid-0-t-0,#steps-uid-0-t-1,#steps-uid-0-t-2,#steps-uid-0-t-3{
		width:100%!important;
		border-radius: 0px;

	}
	.wizard > .actions a, .wizard > .actions a:hover, .wizard > .actions a:active{
		    background: #02c0ce!important;
    color: #fff;
    border-radius: 2px!important;
	}
	.buttonsubmit{
			    background: #02c0ce!important;
    color: #fff;
    border-radius: 2px!important;
	}
	.wizard > .actions .disabled a, .wizard > .actions .disabled a:hover, .wizard > .actions .disabled a:active{
		background: #f2f2f2!important;
    color: #313a46!important;

	}
	.wizard > .steps .done a, .wizard > .steps .done a:hover, .wizard > .steps .done a:active{
		    background: #e3eaef!important;
		    color:black;
}
.wizard > .steps {
    height: 58px!important;
}
.steps > ul{
	margin-left: -10px!important;
}
.wizard > .steps .disabled a, .wizard > .steps .disabled a:hover, .wizard > .steps .disabled a:active{
    color: #313a46!important;

}
.Finish{
	display: none!important;
}
</style>
	  <!-- Start Page content -->
                <div class="content">
                    <div class="container-fluid">

                        <!-- Basic Form Wizard -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card-box">
                                    <p class="text-muted m-b-30 font-13">
                                        (*) are Mandatory
                                    </p>

                                    <div class="pull-in">
                                        <form id="basic-form" action="{{route('branches.store')}}" method="post" enctype="multipart/form-data">
                                            <div>
                                            	{{ csrf_field() }}
                                                <h3>Branch Info</h3>
                                                <section>									
                                                	<div class="row">
                                                		<div class="col-md-4">
                                                				<div class="form-group clearfix">
			                                                        <label class="control-label " for="confirm">Name *</label>
			                                                        <div class="">
			                                                            <input id="branchname" name="branchname" type="text" class="required form-control">
			                                                        </div>
			                                                    </div>
                                                		</div>
                                                		<div class="col-md-4">
                                                				<div class="form-group clearfix">
			                                                        <label class="control-label " for="confirm">Code *</label>
			                                                        <div class="">
			                                                            <input id="branchcode" name="branchcode" type="text" class="required form-control">
			                                                        </div>
			                                                    </div>
                                                		</div>
                                                		<div class="col-md-4">
                                                				<div class="form-group clearfix">
			                                                        <label class="control-label " for="confirm">Phone Number *</label>
			                                                        <div class="">
			                                                            <input id="branchphoneno" name="branchphoneno" type="text" class="required form-control">
			                                                        </div>
			                                                    </div>
                                                		</div>
                                                		<div class="col-md-4">
                                                				<div class="form-group clearfix">
			                                                        <label class="control-label " for="confirm">Email  *</label>
			                                                        <div class="">
			                                                            <input id="branchemail" name="branchemail" type="text" class="required form-control">
			                                                        </div>
			                                                    </div>
                                                		</div>
                                                		<div class="col-md-4">
                                                				<div class="form-group clearfix">
			                                                        <label class="control-label " for="confirm">No Shifts </label>
			                                                        <div class="">
			                                                            <input id="shifts" name="shifts" type="text" class="required form-control">
			                                                        </div>
			                                                    </div>
                                                		</div>
                                                		<div class="col-md-4">
                                                				<div class="form-group clearfix">
			                                                        <label class="control-label " for="confirm"> Organisation </label>
			                                                        <div class="">

			                                                        	@if(Auth::user()->department == "Super Admin")
			                                                        		<input id="Organisationid" name="Organisationid" type="text" class="required form-control" value="">
			                                                        	@elseif(Auth::user()->department == "Admin")
			                                                        		<input id="Organisationid" name="Organisationid" type="text" class="required form-control" value="{{Auth::user()->id}}" readonly>
                                                                        @elseif(Auth::user()->department == "Sub Admin")
                                                                        <input id="Organisationid" name="Organisationid" type="text" class="required form-control" value="{{Auth::user()->organisationid}}" readonly>
                                                                        @else
                                                                            <input id="Organisationid" name="Organisationid" type="text" class="required form-control" value="{{Auth::user()->organisationid}}" readonly>
			                                                        	@endif
			                                                            
			                                                        </div>
			                                                    </div>
                                                		</div>
                                                		<div class="col-md-4">
                                                				<div class="form-group clearfix">
			                                                        <label class="control-label " for="confirm">Default Shifts </label>
			                                                        <div class="">
			                                                            <input id="defaultshifts" name="defaultshifts" type="text" class="required form-control">
			                                                        </div>
			                                                    </div>
                                                		</div>
                                                		<div class="col-md-4">
                                                					<div class="checkbox checkbox-primary">
					                                                    <input id="checkbox21" type="checkbox" name="extrapay">
					                                                    <label for="checkbox21">
					                                                        Extra Pay
					                                                    </label>
					                                                </div>
                                                			</div>
                                                			<div class="col-md-4">
                                                					<div class="checkbox checkbox-primary">
					                                                    <input id="checkbox212" type="checkbox" name="flexiblework">
					                                                    <label for="checkbox212">
					                                                        Flexible Work
					                                                    </label>
					                                                </div>
                                                			</div>
                                                		<div class="col-md-4">
                                                				<div class="form-group">
			                                                        <label class="col-2 col-form-label">Address</label>
			                                                        <div class="col-10">
			                                                            <textarea class="form-control" rows="5" name="branchaddress"></textarea>
			                                                        </div>
			                                                    </div>
                                                		</div>
                                                	</div>
                                                 </section>
                                              
                                                
                                            </div>
                                        </form>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <!-- End row -->

                  </div>

               </div>

@include('admin.footer') 


        <!--Form Wizard-->
        <script src="{{ URL::asset('dashboard/plugins/jquery.steps/js/jquery.steps.min.js') }}" type="text/javascript"></script>

        <!--wizard initialization-->
        <script src="{{ URL::asset('dashboard/assets/pages/jquery.wizard-init.js') }}" type="text/javascript"></script>


             <!-- plugin js -->
        <script src="{{ URL::asset('dashboard/plugins/moment/moment.js') }}"></script>
        <script src="{{ URL::asset('dashboard/plugins/bootstrap-timepicker/bootstrap-timepicker.js') }}"></script>
        <script src="{{ URL::asset('dashboard/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js') }}"></script>
        <script src="{{ URL::asset('dashboard/plugins/clockpicker/js/bootstrap-clockpicker.min.js') }}"></script>
        <script src="{{ URL::asset('dashboard/plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
        <script src="{{ URL::asset('dashboard/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>


        <script src="{{ URL::asset('dashboard/plugins/switchery/switchery.min.js') }}"></script>
        <script src="{{ URL::asset('dashboard/plugins/bootstrap-tagsinput/js/bootstrap-tagsinput.min.js') }}"></script>
        <script src="{{ URL::asset('dashboard/plugins/select2/js/select2.min.js') }}" type="text/javascript"></script>
        <script src="{{ URL::asset('dashboard/plugins/bootstrap-select/js/bootstrap-select.js') }}" type="text/javascript"></script>
        <script src="{{ URL::asset('dashboard/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js') }}" type="text/javascript"></script>
        <script src="{{ URL::asset('dashboard/plugins/bootstrap-maxlength/bootstrap-maxlength.js') }}" type="text/javascript"></script>

        <script type="text/javascript" src="{{ URL::asset('dashboard/plugins/autocomplete/jquery.mockjax.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('dashboard/plugins/autocomplete/jquery.autocomplete.min.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('dashboard/plugins/autocomplete/countries.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('dashboard/assets/pages/jquery.autocomplete.init.js') }}"></script>

        <!-- Init js -->
        <script src="{{ URL::asset('dashboard/assets/pages/jquery.form-pickers.init.js') }}""></script>
        <script type="text/javascript">
            $(document).ready(function() {
            		$(".submit").click(function(event){
  							event.preventDefault();

					});
              	
            		$(".actions ul").append('<li><input type="submit" value="Submit" class="submit btn btn-custom waves-light waves-effect"></li>');
              	

             
              
              
             
            } );

        </script>

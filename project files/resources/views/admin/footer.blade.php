 <footer class="footer">
                    2019 © Site-Galleria
                </footer>

            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


        </div>
        <!-- END wrapper -->



        <!-- jQuery  -->
        <script src="{{ URL::asset('dashboard/assets/js/jquery.min.js') }}"></script>
        <script src="{{ URL::asset('dashboard/assets/js/bootstrap.bundle.min.js') }}"></script>
        <script src="{{ URL::asset('dashboard/assets/js/metisMenu.min.js') }}"></script>
        <script src="{{ URL::asset('dashboard/assets/js/waves.js') }}"></script>
        <script src="{{ URL::asset('dashboard/assets/js/jquery.slimscroll.js') }}"></script>

        <!-- Flot chart -->
        <script src="{{ URL::asset('dashboard/plugins/flot-chart/jquery.flot.min.js') }}"></script>
        <script src="{{ URL::asset('dashboard/plugins/flot-chart/jquery.flot.time.js') }}"></script>
        <script src="{{ URL::asset('dashboard/plugins/flot-chart/jquery.flot.tooltip.min.js') }}"></script>
        <script src="{{ URL::asset('dashboard/plugins/flot-chart/jquery.flot.resize.js') }}"></script>
        <script src="{{ URL::asset('dashboard/plugins/flot-chart/jquery.flot.pie.js') }}"></script>
        <script src="{{ URL::asset('dashboard/plugins/flot-chart/jquery.flot.crosshair.js') }}"></script>
        <script src="{{ URL::asset('dashboard/plugins/flot-chart/curvedLines.js') }}"></script>
        <script src="{{ URL::asset('dashboard/plugins/flot-chart/jquery.flot.axislabels.js') }}"></script>

        <!-- KNOB JS -->
        <!--[if IE]>
        <script type="text/javascript" src="../plugins/jquery-knob/excanvas.js"></script>
        <![endif]-->
        <script src="{{ URL::asset('dashboard/plugins/jquery-knob/jquery.knob.js') }}"></script>

        <!-- Dashboard Init -->
        <script src="{{ URL::asset('dashboard/assets/pages/jquery.dashboard.init.js') }}"></script>

        <!-- App js -->
        <script src="{{ URL::asset('dashboard/assets/js/jquery.core.js') }}"></script>
        <script src="{{ URL::asset('dashboard/assets/js/jquery.app.js') }}"></script>

    </body>
</html>
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Employee;
use App\employee_info;
use App\bank_deatial;
use DB;
use App\billing;
use App\branch;
class employeescontorller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function index()
    {
        //
          $department = Auth::user()->department;
          switch ($department) {
              case 'Super Admin':
                                    $employess = Employee::where('department' , '!=','Super Admin')->get();
                                    $permissionset =explode(",", Auth::user()->permissions);
                                    break;
             case 'Admin':
                            $organisationid = Auth::user()->id;
                            $permissionset =explode(",", Auth::user()->permissions);
                            $employess = DB::table('employees')
                                        ->select('employees.*')
                                        ->where('department', '!=','Admin')
                                        ->where('organisationid',$organisationid)
                                        ->get();
                  break;
              case 'Sub Admin':  $permissionset =explode(",", Auth::user()->permissions);
                                 $organisationid = Auth::user()->organisationid;
                                 $branch = Auth::user()->branch;
                                 $employess = DB::table('employees')
                                        ->select('employees.*')
                                        ->where('department', '!=','Admin')
                                        ->where('department', '!=','Sub Admin')
                                        ->where('organisationid',$organisationid)
                                        ->where('branch',$branch)
                                        ->get();
                                break;
              default:
                   $permissionset =explode(",", Auth::user()->permissions);
                                 $organisationid = Auth::user()->organisationid;
                                 $branch = Auth::user()->branch;
                                 $employess = DB::table('employees')
                                        ->select('employees.*')
                                        ->where('department', '!=','Admin')
                                        ->where('department', '!=','Sub Admin')
                                        ->where('employees.id', '!=',Auth::user()->id)
                                        ->where('organisationid',$organisationid)
                                        ->where('branch',$branch)
                                        ->get();
                  break;
          }

        
         
         if(in_array("2", $permissionset)){
                return view('admin/Employee/index',compact('employess','permissionset'));
         }else{
            return "dnt have permission";
         }
         
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
         $permissionset =explode(",", Auth::user()->permissions);

         $department = Auth::user()->department;
         $branches = null;
          switch ($department) {
              case 'Super Admin':
                                    $departments= "";
                                    break;
             case 'Admin':
                            $departments= "";
                            $id = Auth::user()->id;
                            $branches = branch::where('organisation_id',$id)->get();

                  break;
              case 'Sub Admin':  
                                 $organisationid = Auth::user()->organisationid;
                                 $branch = Auth::user()->branch;
                                 $departments = DB::table('departments')
                                        ->select('departments.*')
                                        ->where('organisation_id',$organisationid)
                                        ->where('branch',$branch)
                                        ->get();
                                    
                                break;
              default:
                  $organisationid = Auth::user()->organisationid;
                                 $branch = Auth::user()->branch;
                                 $departments = DB::table('departments')
                                        ->select('departments.*')
                                        ->where('organisation_id',$organisationid)
                                        ->where('branch',$branch)
                                        ->get();
                  break;
          }
          if(in_array("1", $permissionset)){
                 return view('admin/Employee/create',compact('permissionset','departments','branches'));
         }else{
            return "dnt have permission";
         }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        //dd($request->all());
         $filename = NULL;
        
      
        if($request->hasFile('profileimage'))  {
           // $imagename =  $request->profileimage->getClientOriginalName();
           //  $imagepath = $request->file('profileimage')->storeAs('public', $imagename);
           //  $imagepath = explode("/",$imagepath);
           //  $finalpath = $imagepath[1];

            $file = $_FILES['profileimage'];
            $filetmpname = $_FILES['profileimage']['tmp_name'];
            $filename = $_FILES['profileimage']['name'];
            $target = 'images/'.$filename;
            move_uploaded_file( $filetmpname, $target);
        }
     

        $Employee = new Employee;
        $Employee->name =  $request->fullname;
        $Employee->category =  $request->Category;
        $Employee->dob =  $request->dob;
        $Employee->gender =  $request->gender;
         if($request->Department == "Admin" || $request->Department == "Sub Admin"){
            $department = $request->Department;
            $Employee->organisationid =  null;
        }else{
            $departmentarray = explode("-", $request->Department);
            $Employee->organisationid =  $request->Organisation;
            $department = $departmentarray[0];
        }
        $Employee->department =  $department;
        $Employee->designation =  $request->Designation;
        $Employee->email =  $request->email;
        $Employee->password = bcrypt($request->Password); 
        $Employee->phoneno =  $request->phoneno;
        $Employee->altphoneno =  $request->altphoneno;
        $Employee->joiningdate =  $request->JoiningDate;
        $Employee->employeetype =  $request->type;
        $Employee->image =  $filename;
        $Employee->workinghour =  $request->workinghour;
        $Employee->workperweek =  $request->workingperweek;
        $Employee->branch =  $request->clientbranch;
        $Employee->permissions =  $request->Permissions;

        
        $Employee->supervisor =  $request->Supervisor;
        if($request->Active == "on"){
            $value = 1;
        }else{
            $value = 0;
        }
        $Employee->active = $value ;
        $Employee->save();
        $employee=DB::table('employees')->max('id');
        $id=$employee;

        $filenameidentityproof=NULL;
         if($request->hasFile('identityproof'))  {

            $fileidentityproof = $_FILES['identityproof'];
            $filetmpnameidentityproof = $_FILES['identityproof']['tmp_name'];
            $filenameidentityproof = $_FILES['identityproof']['name'];
            $targetidentityproof = 'images/'.$filenameidentityproof;
            move_uploaded_file( $filetmpnameidentityproof, $targetidentityproof);
        }
        $filenameaddressproof = NULL;
         if($request->hasFile('addressproof'))  {
            $fileaddressproof = $_FILES['addressproof'];
            $filetmpnameaddressproof = $_FILES['addressproof']['tmp_name'];
            $filenameaddressproof = $_FILES['addressproof']['name'];
            $targetaddressproof = 'images/'.$filenameaddressproof;
            move_uploaded_file( $filetmpnameaddressproof, $targetaddressproof);
        }

        $employee_info = new employee_info;

        $employee_info->empid  =$id;
        $employee_info->house_no  = $request->houseno;
        $employee_info->locality  = $request->locality;
        $employee_info->city  =$request->city;
        $employee_info->state  =$request->state;
        $employee_info->country  =$request->country;
        $employee_info->pincode  =$request->pincode;
        $employee_info->qualification  =$request->Qualification;
        $employee_info->identity_proof  =$request->filenameidentityproof;
        $employee_info->addr_proof  =$request->$filenameaddressproof;
        $employee_info->NRIC_FIN_PassportNo  =$request->NRICFINPassportNo;
        $employee_info->EmployeeTaxRefNo  =$request->EmployeeTaxRefNo;
        $employee_info->EmployeeCPFAccountNo  =$request->EmployeeCPFAccountNo;
        $employee_info->Probationperiod  =$request->probationperiod;
        $employee_info->Noticeperiod  =$request->noticeperiod;
        $employee_info->basic_salary  =$request->salary;
        $employee_info->save();

         $bank_deatial = new bank_deatial;
         $bank_deatial->empid =$id;
         $bank_deatial->bank_name =$request->bankname;
         $bank_deatial->accountno =$request->accountno;
         $bank_deatial->bank_branch =$request->bankbranch;
         $bank_deatial->bank_city =$request->bankbranch;
         $bank_deatial->swift_code =$request->swiftcode;
         $bank_deatial->save();
       
        

        if($request->Department == "Admin"){
            $billing = new billing;
            $billing->organisation_id  = $id;
            $billing->package_id   = 1;
            $billing->start_date   = $request->packagesatrtdate;
            $billing->end_date   = $request->packageenddate;
            $billing->save();

            $branch = new branch;
            $branch->branchname =$request->clientbranch;
            $branch->branch_code = NULL;
            $branch->address  = NULL;
            $branch->phone_no =$request->phoneno;
            $branch->email_id = $request->email;
            $branch->no_shifts =NULL;
            $branch->default_shifts =NULL;
            $branch->flexible_work =NULL;
            $branch->extra_pay =NULL;
            $branch->organisation_id = $id;
            $branch->created_by =Auth::user()->department;
            $branch->save();
        }
        return redirect('employees');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

        $organisationid = $id;
        $employeeid = $id;
        // $query =  DB::table('employees')
        //             ->select('employees.*','employee_infos.*','bank_deatials.*')
        //             ->join('employee_infos', 'employee_infos.empid', '=', 'employees.id')
        //             ->join('bank_deatials', 'bank_deatials.empid', '=', 'employees.id')
        //             ->where('employees.id',$organisationid);
        $query = DB::table('employees')
                    ->select('employees.*','employee_infos.*','bank_deatials.*')
                    ->join('employee_infos', 'employee_infos.empid', '=', 'employees.id')
                    ->join('bank_deatials', 'bank_deatials.empid', '=', 'employees.id');
                    
         $department = Auth::user()->department;
         $packagedeatils = null;
          switch ($department) {
              case 'Super Admin':
                                    $employeedeatils = Employee::where('id',$id)->first();
                                    if($employeedeatils->department == "Admin"){
                                       $employee = $query->where('employees.id',$employeeid)
                                                ->first();
                                        $packagedeatils =  DB::table('packages')
                                                        ->select('packages.*','billings.*')
                                                        ->join('billings', 'billings.package_id', '=', 'packages.id')
                                                        ->where('billings.organisation_id',$organisationid)
                                                        ->first();
                                    }else{
                                        $employee = $query->where('employees.id',$employeeid)
                                                ->first();
                                    }

                                    break;
             case 'Admin':
                           
                                $employee =  $query->where('employees.id',$employeeid)
                                                ->first();
                  break;
              case 'Sub Admin':
                                $employee =  $query->where('employees.id',$employeeid)
                                                    ->first();
                    
                                break;
              default:
                  $employee =  $query->where('employees.id',$employeeid)
                                                    ->first();
                  break;
          }
          $permissionset =explode(",", Auth::user()->permissions);
          if(in_array("3", $permissionset)){
                 return view('admin/Employee/edit',compact('permissionset','employee','packagedeatils','employeeid'));
         }else{
            return "dnt have permission";
         }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $Employee = Employee::find($id);
        $Employee->name =  $request->fullname;
        $Employee->category =  $request->Category;
        $Employee->dob =  $request->dob;
        $Employee->gender =  $request->gender;
        $Employee->designation =  $request->Designation;
        $Employee->email =  $request->email;
        $Employee->phoneno =  $request->phoneno;
        $Employee->altphoneno =  $request->altphoneno;
        $Employee->joiningdate =  $request->JoiningDate;
        $Employee->employeetype =  $request->type;
        
        $Employee->workinghour =  $request->workinghour;
        $Employee->workperweek =  $request->workingperweek;
        $Employee->organisationid =  $request->Organisation;
        $Employee->supervisor =  $request->Supervisor;
        if($request->Active){
            $value = 1;
        }else{
            $value = 0;
        }
        $Employee->active = $value ;
        $Employee->save();

        $employee_info = employee_info::where('empid',$id)->first();
       $employee_info->house_no  = $request->houseno;
        $employee_info->locality  = $request->locality;
        $employee_info->city  =$request->city;
        $employee_info->state  =$request->state;
        $employee_info->country  =$request->country;
        $employee_info->pincode  =$request->pincode;
        $employee_info->qualification  =$request->Qualification;
      
        $employee_info->NRIC_FIN_PassportNo  =$request->NRICFINPassportNo;
        $employee_info->EmployeeTaxRefNo  =$request->EmployeeTaxRefNo;
        $employee_info->EmployeeCPFAccountNo  =$request->EmployeeCPFAccountNo;
        $employee_info->Probationperiod  =$request->probationperiod;
        $employee_info->Noticeperiod  =$request->noticeperiod;
        $employee_info->basic_salary  =$request->salary;
        $employee_info->save();

        
        $bank_deatial = bank_deatial::where('empid',$id)->first();
        
         $bank_deatial->bank_name =$request->bankname;
         $bank_deatial->accountno =$request->accountno;
         $bank_deatial->bank_branch =$request->bankbranch;
         $bank_deatial->bank_city =$request->bankbranch;
         $bank_deatial->swift_code =$request->swiftcode;
         $bank_deatial->save();

          if($request->Department == "Admin"){
            $billing = billing::where('organisation_id',$id)->first();
            $billing->start_date   = $request->packagesatrtdate;
            $billing->end_date   = $request->packageenddate;
            $billing->save();

        }
       return redirect('employees');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\branch;
use Illuminate\Support\Facades\Auth;
use DB;
class branchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function index()
    {
        //
         $department = Auth::user()->department;
         $query =  DB::table('branches')
                    ->select('branches.*','employees.name')
                    ->join('employees', 'branches.organisation_id', '=', 'employees.id');
          switch ($department) {

              case 'Super Admin':
                                    $branches = $query->get();
                                    $permissionset =explode(",", Auth::user()->permissions);
                                    break;
             case 'Admin':
                            $organisationid = Auth::user()->id;
                            $permissionset =explode(",", Auth::user()->permissions);
                            $branches =  $query->where('branches.organisation_id',$organisationid)
                                                ->get();
                  break;
              case 'Sub Admin':
                            $permissionset =explode(",", Auth::user()->permissions);
                            $organisationid = Auth::user()->organisationid;
                            $branches =  $query->where('branches.organisation_id',$organisationid)
                                                ->get();
                        break;
              default:
                  $permissionset =explode(",", Auth::user()->permissions);
                            $organisationid = Auth::user()->organisationid;
                            $branches =  $query->where('branches.organisation_id',$organisationid)
                                                ->get();
                  break;
          }

        
         
         if(in_array("9", $permissionset)){
                return view('admin/Branches/index',compact('branches','permissionset'));
         }else{
            return "dnt have permission";
         }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $department = Auth::user()->department;
          switch ($department) {

              case 'Super Admin':
                                    $permissionset =explode(",", Auth::user()->permissions);
                                    break;
             case 'Admin':
                            $organisationid = Auth::user()->id;
                            $permissionset =explode(",", Auth::user()->permissions);
                            
                  break;
              case 'Sub Admin': 
                             $permissionset =explode(",", Auth::user()->permissions);
                            $organisationid = Auth::user()->organisationid;
                            break;
              default:
                   $permissionset =explode(",", Auth::user()->permissions);
                            $organisationid = Auth::user()->organisationid;
                  break;
          }
          if(in_array("10", $permissionset)){
                return view('admin/Branches/create',compact('permissionset'));
         }else{
            return "dnt have permission";
         }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $department = Auth::user()->department;
        $branch = new branch;
        $branch->branchname  = $request->branchname;
        $branch->branch_code  = $request->branchcode;
        $branch->address  = $request->branchaddress;
        $branch->phone_no  = $request->branchphoneno;
        $branch->email_id  = $request->branchemail;
        $branch->no_shifts  = $request->shifts;
        $branch->default_shifts  = $request->defaultshifts;
        $branch->organisation_id  = $request->Organisationid;
        $branch->created_by  = $department;
        if($request->flexiblework  == "on"){
            $flexiblework = 1;
        }else{
            $flexiblework = 0;
        }
        $branch->flexible_work  = $flexiblework;
        if($request->extrapay  == "on"){
            $extra_payvalue = 1;
        }else{
            $extra_payvalue = 0;
        }
        $branch->extra_pay  = $extra_payvalue;
        $branch->save();
        return redirect("/branches");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $permissionset =explode(",", Auth::user()->permissions);
        $branch = branch::find($id);
        if(in_array("11", $permissionset)){
                return view('admin/Branches/edit',compact('permissionset','branch'));
         }else{
            return "dnt have permission";
         }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $branch = branch::find($id);
         $branch->branchname  = $request->branchname;
        $branch->branch_code  = $request->branchcode;
        $branch->address  = $request->branchaddress;
        $branch->phone_no  = $request->branchphoneno;
        $branch->email_id  = $request->branchemail;
        $branch->no_shifts  = $request->shifts;
        $branch->default_shifts  = $request->defaultshifts;
        $branch->organisation_id  = $request->Organisationid;
        if($request->flexiblework  == "on"){
            $flexiblework = 1;
        }else{
            $flexiblework = 0;
        }
        $branch->flexible_work  = $flexiblework;
        if($request->extrapay  == "on"){
            $extra_payvalue = 1;
        }else{
            $extra_payvalue = 0;
        }
        $branch->extra_pay  = $extra_payvalue;
        $branch->save();
        return redirect("/branches");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }



    public function getorganiationbranches(Request $request){
       $organisationid = $request->id;
       $query =  DB::table('branches')
                    ->select('branches.*','employees.name')
                    ->join('employees', 'branches.organisation_id', '=', 'employees.id');
          $branches =  $query->where('branches.organisation_id',$organisationid)->get();
      return view('admin/Departments/branches',compact('branches'));
    }
      public function testing(Request $request){
       $organisationid = $request->xml;
       return $organisationid;
    }
}

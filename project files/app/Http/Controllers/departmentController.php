<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use App\department;

class departmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function index()
    {
        //
         $department = Auth::user()->department;
         $query =  DB::table('departments')
                    ->select('departments.*','employees.name')
                    ->join('employees', 'departments.organisation_id', '=', 'employees.id');

         switch ($department) {
              case 'Super Admin':
                                    $departments = $query->get();
                                    $permissionset =explode(",", Auth::user()->permissions);
                                    break;
             case 'Admin':
                            $organisationid = Auth::user()->id;
                            $permissionset =explode(",", Auth::user()->permissions);
                            $departments = $query->where('departments.organisation_id',$organisationid)->get();
                            
                  break;
              case 'Sub Admin': 
                            $organisationid = Auth::user()->organisationid;
                            $permissionset =explode(",", Auth::user()->permissions);
                            $branch =explode(",", Auth::user()->branch);
                            $departments = $query->where('departments.organisation_id',$organisationid)
                                                    ->where('departments.branch',$branch)
                                                 ->get();
                        break;
              default:
                            $organisationid = Auth::user()->organisationid;
                            $permissionset =explode(",", Auth::user()->permissions);
                            $branch =explode(",", Auth::user()->branch);
                            $departments = $query->where('departments.organisation_id',$organisationid)
                                                    ->where('departments.branch',$branch)
                                                 ->get();
                  break;
          }
          return view('admin/Departments/index',compact('permissionset','department','departments'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $permissionset =explode(",", Auth::user()->permissions);
        $department = Auth::user()->department;
        $branch = null;
        $branches = null;
           switch ($department) {

              case 'Super Admin':
                                    $permissionset =explode(",", Auth::user()->permissions);
                                    $branches = "";
                                    break;
             case 'Admin':
                            $organisationid = Auth::user()->id;
                             $query =  DB::table('branches')
                                        ->select('branches.*','employees.name')
                                        ->join('employees', 'branches.organisation_id', '=', 'employees.id');
                              $branches =  $query->where('branches.organisation_id',$organisationid)->get();
                       
                            $permissionset =explode(",", Auth::user()->permissions);
                            
                  break;
               case 'Sub Admin': 
                            $organisationid = Auth::user()->organisationid;
                            $permissionset =explode(",", Auth::user()->permissions);
                            $branch = Auth::user()->branch;
                            break;
              default:
                  # code...
                  break;
          }
          if(in_array("6", $permissionset)){
             return view('admin/Departments/create',compact('permissionset','branches','branch'));
         }else{
            return "dnt have permission";
         }
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $department = new department;
        $department->deptname = $request->deptname;
        $department->permissions = $request->Permissions;
        $department->organisation_id = $request->Organisationid;
        $department->branch = $request->branch;
        $department->code = $request->deptcode;
        $department->save();
        return redirect('/departments'); 

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $permissionset =explode(",", Auth::user()->permissions);
        $department = department::find($id);
        $query =  DB::table('branches')
                    ->select('branches.*','employees.name')
                    ->join('employees', 'branches.organisation_id', '=', 'employees.id');
        $branches =  $query->where('branches.organisation_id',$department->organisation_id)->get();
        return view('admin/Departments/edit',compact('permissionset','department','branches'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $department = department::find($id);
     
        $department->deptname = $request->deptname;
        $department->permissions = $request->Permissions;
        $department->organisation_id = $request->Organisationid;
        $department->branch = $request->branch;
        $department->code = $request->deptcode;
        $department->save();
        return redirect('/departments'); 

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use App\billing;
class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
            $department = Auth::user()->department;

            switch ($department) {
                case 'Super Admin':
                                    $permissionset =explode(",", Auth::user()->permissions);
                                    return view('admin.home',compact('permissionset'));
                                    break;
                case 'Admin':
                                     $permissionset =explode(",", Auth::user()->permissions);
                                     $id = Auth::user()->id;
                                     $billingvalue = self::billingverification($id);
                                     $Active = Auth::user()->active;
                                     if($Active == 1 && $billingvalue == 1){
                                        return view('admin.home',compact('permissionset'));
                                     }else{
                                        dd("Your Susbscription is over");
                                     }
                                     
                                     

                                    break;
                case 'Sub Admin':  
                                     $permissionset =explode(",", Auth::user()->permissions);
                                     $id = Auth::user()->id;
                                     $organisation_id = Auth::user()->organisationid;
                                     $billingvalue = self::billingverification($organisation_id);
                                     $Active = Auth::user()->active;
                                     if($Active == 1 && $billingvalue == 1){
                                        return view('admin.home',compact('permissionset'));
                                     }else{
                                        dd("Your Susbscription is over");
                                     }
                                    break;
                default:
                                     $permissionset =explode(",", Auth::user()->permissions);
                                     $id = Auth::user()->id;
                                     $organisation_id = Auth::user()->organisationid;
                                     $billingvalue = self::billingverification($organisation_id);
                                     $Active = Auth::user()->active;
                                     if($Active == 1 && $billingvalue == 1){
                                        return view('admin.home',compact('permissionset'));
                                     }else{
                                        dd("Your Susbscription is over");
                                     }
                    break;
            }
            
            
    }

    public function billingverification($id){
             $billingdetails = billing::where('organisation_id',$id)->first();
            $startdate = $billingdetails->start_date;
            $enddate = $billingdetails->end_date;
            $currentDate = date('Y-m-d');
            $contractDateBegin = date('Y-m-d', strtotime($startdate));
            $contractDateEnd = date('Y-m-d', strtotime($enddate));
            if (($currentDate >= $contractDateBegin) && ($currentDate <= $contractDateEnd)){
                return 1;
            }else{
                return 0; 
            }
    }
}

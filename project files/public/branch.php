<?php

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "sitegalleriahrms";
$organisation_id = $_GET['str'];
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
$sql = "SELECT * FROM branches
		where branches.organisation_id=".$organisation_id;
$result = $conn->query($sql);
?>

<select class="form-control" id="branchesonload">

<?php
if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        	$value =  $row["organisation_id"]."-".$row["branchname"];
        ?>

        <option value="<?php echo $value;?>"><?php echo $row["branchname"];?></option>

        <?php 
    }
} else {
   
}
?>
</select>

<script type="text/javascript">
	  $('#branchesonload').change(function() {
             	let value = $(this).val();
             	let valuesplit = value.split("-");
             	let organisation_id = valuesplit[0];
             	let branchselected = valuesplit[1];

             	 var xhttp = new XMLHttpRequest();
						  xhttp.onreadystatechange = function() {
						    if (this.readyState == 4 && this.status == 200) {
						   
						     
						     $(".departmentss").html(this.responseText);
						    }
						  };
						  xhttp.open("GET", "http://127.0.0.1:8000/departments.php?organisation_id="+organisation_id+"&brnach="+branchselected, true);
						  xhttp.send();
             });
</script>
<?php
$conn->close();
?>